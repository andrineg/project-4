# Project - 4
For this assigment we have choosen to rework project 3 with more focus on sustainablity. As our last project had a bad practis with a lot of unnecessary code and lots of unused potential, have we now choosen an easier and more static page. 
 We have ended up using most of the time just removing and reworking a lot from last time. We have also decided to ditch the light/dark theme and just have a darker over all theme to save power etc.

# Needed to be installed in order to run

## To start, run the following commands:
```
cd client
npm start
cd server
npm start

```

## How to run snapshot and unit tests:
```
cd client
npm start
npm test

```

## How to run e2e-test :
```
open three different terminals:

cd client
npm start

cd server
npm start

cd client
npx cypress open
```

### The project structure
We have divided our project into two parts: a Client and a Server.  The backend side consists of mongoose models, resolvers and files for starting the Apollo server. Our application is connected to a MongoDB database that on the virtual machine. The frontend or client consists of the application itself, with different views containing presentational components. 

## Backend- Server
The file index.ts creates an Apollo Server and starts it using startStandaloneServer, which uses Apollo's Express integration, expressMiddleware. This way we did not have to configure Express. This gives us a very simple setup. There are several GraphQL queries that are linked to the same type of data. Therefore, the suggestion is to include all queries and mutations in GraphQL and MongoDB in the same resolver for easy integration. It is clear that all types and mutations are relatively similar, and there are not many, so we believe that it is the most important to summarize this in a comprehensive manner.
Database The database consists of a single mongodb instance that is installed on the local machine of the virtual machine.

### MongoDB 
MongoDB may be a suitable database for use in a web application for a number of reasons. MongoDB was chosen because of its ease of deployment, administration, and scalability. A benefit of MongoDB is that it is a NoSQL database, which allows it to be more flexible than other traditional SQL databases. If your data is not well defined or if you need to change the database structure quickly, this can be very useful. MongoDB can also handle large amounts of data since it is easy to scale. As a final point, MongoDB is known for its reliability and speed.

## Mongoose
Mongoose was chosen for interacting with the database since it facilitates the creation of models for the data. The data can be accessed in a specific manner using this tool. Furthermore, Mongoose provides useful methods for querying and updating data, and we did a lot of reaserch and it was going to be easy to use, and it was well documented.

### GraphQL and Apollo
and Apollo Client for the purpose of importing data from this database. We spent a great deal of time setting up our graphql and dabbling
with the skeleton logic. Our team decided after reading about the different libraries ot use Apollo Client. However, some argue that MobX is easier to understand and implement as a starter. Object-oriented programming is a more familiar concept for beginners, which is why MobX was inspired by it.


## Frontend- Client
The client side of the project is located in the frontend/source directory. Our files are organized into components, Views, services, and styles. We have used React with a few differend libraries, and Apollo client to connect to the API and manage global state and data on the app (apollo refers to what er call global state as local application state). States are managed with the hooks useState and useEffect, and with reactive variables.

### MUI components and libraries
A large portion of the components we used came from the MUI-library. We used the library throughout the project since it has a wide variety of components that are easy to integrate into the design. One of the greatest advantages of the MUI-components is that they offer inline styling, which makes it easier to change the component with CSS. Due to the Box-component, we were able to reduce the number of div-tags since CSS can be applied inline instead of having to create a separate CSS file for each div-tag.

### Universal design
To ensure accessibility we made sure to review the WCAG 2.0 guidelines/success criteria for universal design for web pages. Norwegian national regulations require at least 35 out of in total 61 of these success criteriato be fulfilled, so it was important to us to learn how to do this and implement this in our app. The most prominent requirements we implemented are listed below. Additionally, to ensure we were in alignment with the WCAG 2.0 reccomendations, we used the Google Chrome axe DevTools extension. This tool scanned our web page, and highlighted accessibility problem areas. 

Handicap is a disadvantage attributed to visually impared people, people that are hard of hearing or blind, or that has other disabilities, by societal design that does not accomodate for functional variations. We want everyone to be able to use our application, and not to feel handicapped because they are unable to. It is difficult to accomodate for all functional variations, but we hope that by carefully concidering the above mentioned guidelines, that we can come as close to this goal as possible.

*Colors and contrast*
The first step has been to highlight the text on the elements that lead the user to each of the two pages that comprise the application. Furthermore, we have attempted to avoid using color combinations that pose a challenge to color blind users like green and red. Additionally, we made sure that none of the colors on the page are critical for the user to understand what things are and what actions are possible. For this we have used appropriate icons, texts, titles or lables or other visual (and possibly audible) queues.

*Clickable elements(Navigation)*
Users who lack precision ability are the subject of this requirement. Furthermore, we have kept the search tools relatively large so that users with poor precision won't have difficulty using them. 

We summarize the following points:
- Perceivable 
    - We have emphasized making the application significant and observable
    - We have focused on using a contrast in the text so that it will be easily readable
    - Prioritized the perceived needs for and acceptability of an app that was designed to be a simple travle diary.
    - We tried to reduce the number of div tags in the project, we stribet for a meaningful use landmarks to accomodate for blind users. We aspired to exclusively use div tags for sections of code that were only grouped together for styling purposes, and not functionality. For the same purpose, we made sure to use aria-labels and aria-descriptions in elements that might be anonymous (such as the MUI components, which are rendered to some version of divs).
- Operable - capable of being used
    - There is a natural flow for all the buttons and input fields that occur when the tab key is pressed on a keyboard.
    - It is static pages that do not change before the user clicks on anything. The user decides the action.
    - The web app is easy to navigate in and has easy and understandable icons.
- Understandable
    - We have designed our app to be easy to comprehend
    - Context and relative text structure make the application readable in all kinds of ways.
    - Titles and texts are meaningful.
- Robust 
    - The app has been tested in a variety of ways and environments. We have performed Cross Browser testing, unit tests and end to end test.



## Sustainable web development
Darker colors require less power to display on the screen, and therefore require less energy than light themed applications. We did not use any photos either, and only a limited amount of different colors. This also results in less energy use.

To reduce the amount of data traffic between the client and server side of out application, one thing we did was create a query that only fetches countries by a certain ID from the server. The users travel diary is expected to display a far smaller amount of countries (Country data objects) than the total amount that is saved on the server. To not over fetch data in order to display the users travel diary, this query became very handy. 

We also chose to not use any video the reduce both energy use and data traffic to the client.

Applications with data traffic and less energy use often have a better user experience, as they are quicker than large applications.


## Testing
We tried to test as much as we could, due to the time limit. The fact that our application was not ready untill close to delivery date made testing quite difficult. We are not quite satisfied with all the tests, and an further improvement of the Application would be to write more tests. We preoritized getting in a couple easy  end-to-end tests, and simple unit tests.

In project 3, we conducted end-to-end testing. End-to-end testing refers to the process of testing whether a program is performing as intended - from the beginning to the end. It verifies the entire application / test the various interactions. Our application is written in graphQL. It is therefore natural to use Cypress to test our APIs between clients and servers in an automated manner. We have made an effort to test the website in the hopes that it will be able to be done manually. In the development phase of the project, we have tested the functionality of the website in order to determine if the functionality is what was anticipated. 

We did two snapshot tests, which is essential for determining whether the site perform as planned. In summary, snapshots provide information regarding the status of the user interface if it has changed. Therefore we choose to implement a snapshot test, because we think that it provides necessarily information about rendering the application. We also implemented unit tests, because this is used to test functionality, APIs, and user interactions.

Our application has been tested in Google Chrome, Safari and Mozilla Firefox, and it is responsive with a high level of functionality. The website has also been tested on mobile devices (Google Chrome and Safari) to ensure that it functions as intended. Additionally, we have provided a Lighthouse report for the application in Chrome's developer tools (more on this in the Web accessibility section).

