import CurrentUserProvider from "./context/CurrentUserContext";
import "./style/App.css";
import View from "./View";

const App = () => {
    return (
      <CurrentUserProvider>
        <View/>
      </CurrentUserProvider>
    );
  };
export default App;