import * as React from 'react';
import { styled, ThemeProvider, createTheme } from '@mui/material/styles';
import Box from '@mui/material/Box';
import CssBaseline from '@mui/material/CssBaseline';
import MuiAppBar, { AppBarProps as MuiAppBarProps } from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import TravelExploreIcon from '@mui/icons-material/TravelExplore';

const drawerWidth = 240;
interface AppBarProps extends MuiAppBarProps {
  open?: boolean;
}

const AppBar = styled(MuiAppBar, {
  shouldForwardProp: (prop) => prop !== 'open',
})<AppBarProps>(({ theme, open }) => ({
  transition: theme.transitions.create(['margin', 'width'], {
  
  }),
  ...(open && {
    width: `calc(100% - ${drawerWidth}px)`,
    marginLeft: `${drawerWidth}px`,
    transition: theme.transitions.create(['margin', 'width'], {
    }),
  }),
}));

export default function PersistentDrawerLeft() {
    const colorTheme = createTheme({
        palette: {
          primary: {
            main: '#324736e2',
          },
        },
      });
 
  return (
    <Box sx={{ display: 'flex' }}>
      <CssBaseline />
      <ThemeProvider theme={colorTheme}>
      <AppBar color="primary">
        <Toolbar>
          <Typography  variant="h6" noWrap component="div">
          <TravelExploreIcon sx={{ fontSize: 45 }} />  Travel Diary 
          </Typography>
        </Toolbar>
      </AppBar>  
      </ThemeProvider> 
    </Box>
  );
}