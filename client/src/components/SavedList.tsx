import React, { useState, useEffect, useContext } from 'react'
import { LOAD_USER } from "../GraphQL/Queries";
import { Country, SavedCountry, User } from '../interface/Interfaces';
import { useLazyQuery, useMutation } from "@apollo/client";
import FormControl from '@mui/material/FormControl';
import InputLabel from '@mui/material/InputLabel';
import OutlinedInput from '@mui/material/OutlinedInput';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@mui/icons-material/KeyboardArrowUp';
import "../style/SavedList.css";
import { REMOVE_FAVOURITE, REMOVE_VISITED } from '../GraphQL/Mutations';
import { CurrentUserContext, undefinedUser } from '../context/CurrentUserContext';

export interface SavedListProps {
    countries: SavedCountry[];
    onChange: (country: Country, change: any) => void;
}

//this function displays the SavedCountry elements in the users storedList
const SavedList = () => {
    const [expand, setExpand] = useState<boolean>(false);
    const [editing, setEditing] = useState<boolean>(false);
    const [user, setUser] = useState<String>();
    const {currentUser, setCurrentUser} = useContext(CurrentUserContext);

    const [getUser, {refetch, data: getUserData, loading: getUserLoading, error: getUserError }] = useLazyQuery(LOAD_USER, 
        {  fetchPolicy: "no-cache" });

    const [removeVis, {data: removeVisData}] = useMutation(REMOVE_VISITED);

    const [removeFav, {data: removeFavData}] = useMutation(REMOVE_FAVOURITE);

    const updateEmail = (email: String) => {
        console.log("update email");
        if(email === undefined) {
            email = ""
        }
        setUser(email);
    }

    const handleSubmit = () => {
        setCurrentUser(undefinedUser);
        getUser({variables: {email: user}}).then(data => {
            setCurrentUser(data.data.GetUser);
        })
    }

    const handleupdate = () => {
        refetch({variables: {email: currentUser.email}})
        .then(data => {
            setCurrentUser(data.data.GetUser);
        }
        );
    }

    const handleRemoveFav = (id: Number, name: String) => {
        if (currentUser) {
            removeFav({variables: { 
                email: currentUser.email,
                favourite: {
                    countryID: id,
                    countryName: name
                }
            }}).then(() => {
                handleupdate()
            })
        }
    }

    const handleRemoveVis = (id: Number, name: String) => {
        if (currentUser) {
            removeVis({variables: { 
                email: currentUser.email,
                visited: {
                    countryID: id,
                    countryName: name
                }
            }}).then(() => {
                handleupdate()
            });
        }
    }

    useEffect(() => {
        
    }, [currentUser])
    
    return (
        <div id='savedList' > 
           <FormControl id='loginForm' onSubmit={(e) => {e.preventDefault(); handleSubmit()}}>
              <InputLabel htmlFor="inputEmail">E-mail</InputLabel>
              <OutlinedInput id="inputEmail" onChange={(e) => updateEmail(e.target.value)} label="E-mail*" aria-label="email input field"/>
              <button type="button" onClick={(e) => {
                e.preventDefault();
                handleSubmit();
              }}>Submit</button>
            </FormControl>
            <div className='top' onClick={() =>{setExpand(!expand)}}>
                <h2>My travel diary</h2>
                <p >{expand? <KeyboardArrowUpIcon/> : <KeyboardArrowDownIcon/>}</p>
            </div>

            <div id="favListContent"> <h3>Favorite Countries</h3>
                {expand? 
                <>
                    <>
                        {getUserError && <p> Error when loading data: {getUserError.message} </p>}
                        {getUserLoading && <p> Loading... Please wait. </p> }
                    </>
                    <>
                    {currentUser && 
                        currentUser.favList.map((c: { countryID: number; countryName: string; }) => <li key={c.countryID}>{c.countryName}<button onClick={() => handleRemoveFav(c.countryID, c.countryName)}>Remove</button></li>)
                    }
                    </>
                </>
                :
                null
                }
            </div>
            <div id="visListContent"> <h3>Visited Countries</h3>
                {expand? 
                <>
                    <>
                    {getUserError && <p> Error when loading data: {getUserError.message} </p>}
                        {getUserLoading && <p> Loading... Please wait. </p> }
                    </>
                    <>
                    {currentUser && 
                        currentUser.visList.map((c: { countryID: number; countryName: string; }) => <li key={c.countryID}>{c.countryName}<button onClick={() => handleRemoveVis(c.countryID, c.countryName)}>Remove</button></li>)
                    }
                    </>
                </>
                :
                null
                }
            </div>
        </div>
    )
};
export default SavedList;