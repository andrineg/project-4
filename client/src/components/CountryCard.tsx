import React, { ButtonHTMLAttributes, useEffect, useState } from "react";
import { useReactiveVar } from "@apollo/client";

import Card from "./Card";
import { Country } from "../interface/Interfaces";
import "../style/CountryCard.css";
import Fab from "@mui/material/Fab";
import FavoriteBorderIcon from '@mui/icons-material/FavoriteBorder';
import DoneOutlineIcon from '@mui/icons-material/DoneOutline';



export interface countryCardT {
  country: Country;
  isFav: boolean;
  isVis: boolean;
  countryAction: (countryID: number, countryName: string, event: CountryCardEvent) => void;
};


export enum CountryCardEvent {
   Favourited,
   Visited
};

const CountryCard: React.FC<countryCardT> = ({ country, isFav, isVis, countryAction }) => {
  const [expand, setExpand] = useState<boolean>(false);


  const expanstionText = () =>{
    if (expand){
      return "Hide info";
    } else{
      return "Show more info";
    }
  }


  //CountryCard show different info based on whether countries are favourited/visited or not. 
  //Toggles between expanded and default view on button click.
  return (
    <Card className=" card cardCountry" id={country.countryID.toString()}>
      <div id="defaultView">
        <div id="top">
          <h4 id="name" aria-label='country card' aria-description={country.countryName}>{country.countryName}</h4>
          <>
            <Fab title='Add to favorite' className='icons' id="iconfav" size="medium" color="primary" onClick={() => countryAction(country.countryID, country.countryName, CountryCardEvent.Favourited)} aria-description="Click this button to mark country as favorite" >
              <FavoriteBorderIcon/>
            </Fab>

            <Fab title='Mark as visited' className='icons' id="iconvis" size="medium" color="primary" onClick={() => countryAction(country.countryID, country.countryName, CountryCardEvent.Visited)} aria-description="Click this button to mark country as visited" >
              <DoneOutlineIcon/>
            </Fab>
          </>
        </div>
        <Fab variant="extended" id="iconfav" size="medium" color="primary" onClick={() => {setExpand(!expand)}} aria-description="Click to expand infocard about this country"sx={{width: '100%', height:'5%'}} >
              {expanstionText()}
        </Fab>
      </div>
      <div id="information">
          <p id="region"><b>Continent:</b> {country.region.toString()}</p>
          
        {expand ? (
          <>
            <div className="" id="expanded">
              <p id="population"><b>Population:</b> {country.population.toString()}</p>
              <p id="gdp"><b>GDP (Gross domestic product):</b> {country.gdp.toString()}</p>
              <p id="area"><b>Total land area:</b> {country.area.toString()}</p>
            </div>
          </>
        ) : null}
      </div>
    </Card>
  );
};

export default CountryCard;
