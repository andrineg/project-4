import * as React from "react";
import OutlinedInput from "@mui/material/OutlinedInput";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
import Stack from '@mui/material/Stack';
import Button from '@mui/material/Button';
import Fab from "@mui/material/Fab";

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};

const area = [
  "Africa",
  "Asia",
  "Europe",
  "North America",
  "South America",
  "Oceania",
];

export default function Filter({ setFilter }: any) {
  const [typeName, setTypeName] = React.useState([]);

  const handleChange = (event: any) => {
    const {
      target: { value },
    } = event;
    setTypeName(
      // On autofill we get a the stringified value.
      typeof value === "string" ? value.split(",") : value
    );
    setFilter(value);
  };

  return (
    <div id='Filter'>
      <FormControl sx={{ width: '100%'}} color="success">
        <InputLabel
          id="filterLabel"
          color="secondary"
        >
          Continent
        </InputLabel>
        
        <Select
          labelId="filterLabel"
          id="demo-multiple-name"
          color="secondary"
          multiple
          value={typeName}
          sx={{height: '50%'}}
          onChange={handleChange}
          input={<OutlinedInput label="Name" />}
          MenuProps={MenuProps}
        >
          {area.map((name) => (
            <MenuItem
              key={name}
              value={name}
            >
              {name}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
    </div>
  );
}