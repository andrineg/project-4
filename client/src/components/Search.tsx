import React from 'react';
import TextField from '@mui/material/TextField';

export interface SearchProps {
  onChange: (search: string) => void
}

export default function Search( { onChange } : SearchProps) {
  return (
    <>
      <TextField 
          color='secondary'
          id="filled-basic" 
          label="Search for a country..." 
          variant="filled" 
          onChange={e => onChange(e.target.value)}
          helperText="First letter upper-case, the rest lower-case (Example; Norway)"
      />
    </>
  );
}