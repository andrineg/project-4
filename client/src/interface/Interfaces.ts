export interface Country {
  countryID: number,
  countryName: string,
  region: string,
  population: number,
  area: number,
  gdp: number
}

export interface SavedCountry {
  countryID: number,
  countryName: string
}

export interface User {
  email: string,
  favList: SavedCountry[],
  visList: SavedCountry[]
}