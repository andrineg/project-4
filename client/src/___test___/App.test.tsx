
import {expect, test} from '@jest/globals';
import "@testing-library/jest-dom";
import App from '../App';
import Card from '../components/Card';
import renderer from "react-test-renderer";
import { render, screen } from "@testing-library/react";

test("renders the app without crashing", () => {
  const App = document.createElement("div");
  expect.anything()
});

test("matches snapshot", () => {
  const {container} = render( <Card />);
  expect(container).toMatchSnapshot();
});



