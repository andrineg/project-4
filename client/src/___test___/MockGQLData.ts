
import {LOAD_ALL_COUNTRIES} from "../GraphQL/Queries";
import { MockedProvider } from "@apollo/client/testing";

export const mockedFilteredCCountryQuery = {
    request: {
        query: LOAD_ALL_COUNTRIES,
        variables: {
          countryID: 1,
          countryName: "Afganistan",
          region: "Asia",
          populatio: "31056997",
          area: 647500,
          gdp: 647500
      },
    },
  
};

/*

export const mockedAreaCountryQuery = {
    request: {
        query:LOAD_ALL_COUNTRIES,
        request: {
            query: LOAD_ALL_COUNTRIES,
            variables: {
        countryID: '2',
        countryName: 'Albania',
        region: 'Europe',
        populatio: '3581655',
        area: 28748,
        gdp: 4500
    },
},

},




  countryID: '1',
  countryName: 'Afganistan',
  region: 'Asia',
  populatio: '31056997',
  area: 647500,
  gdp: 647500
},
{
    countryID: '2',
    countryName: 'Albania',
    region: 'Europe',
    populatio: '3581655',
    area: 28748,
    gdp: 4500
},
{
    countryID: '3',
    countryName: 'Algeria',
    region: 'Africa',
    populatio: '32930091',
    area: 2381740,
    gdp: 6000
},
{
    countryID: '4',
    countryName: 'Afganistan',
    region: 'Asia',
    populatio: '31056997',
    area: 647500,
    gdp: 647500
},
{
    countryID: '5',
    countryName: 'American Samoa',
    region: 'Oceania',
    populatio: '31056997',
    area: 199,
    gdp: 8000
},
]


*/