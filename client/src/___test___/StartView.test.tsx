import {expect, jest, test} from '@jest/globals';
import { render, screen } from "@testing-library/react";
import { HashRouter } from "react-router-dom";
import React from "react";
import { ApolloClient, InMemoryCache, ApolloProvider } from '@apollo/client';
import StartView from '../views/StartView';
import configureStore from 'redux-mock-store';
import { Pagination } from '@mui/material';
import renderer from 'react-test-renderer';



// test that the startView renders and dont crash

test("renders startview without crashing", () => {
  const startview = document.createElement("StartView");
  expect.anything()
});


const initialState = {
  email: ""
};

//uses mock to the initialstate

const mockStore = configureStore();
let store: any;
beforeEach(() => {
  store = mockStore(initialState);
});

test ("renders Search correctly", () => { 
  const {container} = render( <Pagination/>);
  expect(container).toMatchSnapshot();
});





/*

describe("when login renders ", () => {
  beforeAll(() => {
  render(
    <ApolloProvider client={client}>
      <React.StrictMode>
        <HashRouter>
          <StartView setEmail={function (email: string): void {
            throw new Error('Function not implemented.');
          } } />
        </HashRouter>
      </React.StrictMode>
    </ApolloProvider>
  )
})


it("checks if the loginPage has an email input and password input", () => {
  const login = screen.getByTestId("emailInput");
  expect(login).toBeInTheDocument();
})

})


*/
