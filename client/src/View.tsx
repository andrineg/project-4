import React, { useContext, useState } from "react";
import "./style/View.css";
import { makeVar, useLazyQuery, useMutation, useQuery } from "@apollo/client";
import {LOAD_ALL_COUNTRIES, LOAD_USER} from "./GraphQL/Queries";
import { Country, User } from "./interface/Interfaces";
import CountryCard, { CountryCardEvent } from "./components/CountryCard";
import SavedList from "./components/SavedList";
import Header from "./components/Header";
import Filter from "./components/Filter";
import Search from "./components/Search"
import Fab from "@mui/material/Fab";
import { iconButtonClasses, Typography } from "@mui/material";
import { ADD_FAVOURITE, ADD_VISITED } from "./GraphQL/Mutations";
import { CurrentUserContext } from "./context/CurrentUserContext";

const View = () => {
  const [isMobile, setIsMobile] = useState(false);
  const regionDefault: string[] = [];

  const [addVis, { data: addVisData}] = useMutation(ADD_VISITED);

  const [addFav, { data: addFavData}] = useMutation(ADD_FAVOURITE);
  
  const {currentUser, setCurrentUser} = useContext(CurrentUserContext);

  const { error, loading, fetchMore, refetch: refetchCountries, data } = useQuery(LOAD_ALL_COUNTRIES, {
    variables: { offset: 0, limit: 10, search: "", sort: {}, region: regionDefault},
  });

const [getUser, {refetch, data: getUserData, loading: getUserLoading, error: getUserError }] = useLazyQuery(LOAD_USER,
  {  fetchPolicy: "no-cache", variables: { email: currentUser.email }});

  const handleupdate = () => {
    refetch()
    .then(data => {
        setCurrentUser(data.data.GetUser);
    }
    );
}

  const handleAddFav = (id: Number, name: String) => {
    if (currentUser) {
      addFav({variables: { 
          email: currentUser.email,
          favourite: {
              countryID: id,
              countryName: name
          }
      }}).then(() => {
        handleupdate();
      });
    }
  }

  const handleAddVis = (id: Number, name: String) => {
    if (currentUser) {
      addVis({variables: { 
          email: currentUser.email,
          visited: {
              countryID: id,
              countryName: name
          }
      }}).then(() => {
        handleupdate()
      });    
    }
  }

  const handleCountryAction = (countryID: number, countryName: string, event: CountryCardEvent) => {
    if(event === 0) {
      handleAddFav(countryID, countryName)
    }
    if(event === 1) {
      handleAddVis(countryID, countryName)
    }
  }

  

    
  return (
      <>
      <div id="View">
      <SavedList/>
        <Header/>
        <div id='searchBox'>
            <Typography id='searchInfo' className='searchEl' variant="h6" component="div" sx={{ flexGrow: 1 }}>Search for a country you have visited or want to visit...</Typography>
            <div id="searchElements">
              <Search onChange={s => refetchCountries({search: s})} />
              {<h4> OR </h4>}
              <Filter id='Filter' setFilter={(r: string[]) => refetchCountries({region: r})} />
          </div>
        </div>
    
        <div id='searchResults'>
          {loading || error ? (
            <>
              {error && <p> Could not load countries: {error.message} </p>}
              {loading && <p> Loading... Please wait.</p>}
            </>
          ) : (
            <>
              <h2>All countries</h2>
              <div className="Countries">
                {data &&
                  data.GetAllCountries.map((country: Country) => {
                    return (
                      <CountryCard
                        key={country.countryID}
                        country={country}
                        isFav={false} 
                        isVis={false} 
                        countryAction={handleCountryAction} />
                      );
                    })}
                {data && (
                
                <Fab variant="extended" id = 'loadMore' size="medium" color="primary" aria-label="Press to load more countries" onClick={async (inView) => {
                  const currentLen = data.GetAllCountries.length || 0;
                  await fetchMore({
                    variables: {
                      offset: currentLen,
                      limit: currentLen+10,
                    },
                  });
                  }} sx={{borderRadius:10, minHeight: '40px', maxHeight: '40px', minWidth: '50%'}}>
                Load More Countries
                </Fab>
                )}
              </div>
            </>
          )}
        </div>
      </div>
  </>
  );
};
export default View;