import React, { Dispatch } from "react";
import { ReactNode, useMemo, useState } from "react";
import { User } from "../interface/Interfaces";

type currentUserContextType = {
    currentUser: User,
    setCurrentUser: Dispatch<User>
}
export const undefinedUser: User = {email: "", favList: [], visList: []}

export const CurrentUserContext = React.createContext<currentUserContextType>({currentUser: undefinedUser, setCurrentUser: () => Promise.resolve()});

function CurrentUserProvider({children}: {children:ReactNode}) {
    const [currentUser, setCurrentUser] = useState<User>(undefinedUser);
    const userProviderValue = useMemo(() => ({currentUser, setCurrentUser}), [currentUser, setCurrentUser]);
    return <CurrentUserContext.Provider value={userProviderValue}>{children}</CurrentUserContext.Provider>
}

export default CurrentUserProvider;
