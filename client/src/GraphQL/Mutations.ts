import {gql} from '@apollo/client';

//Updates user with savedList
export const UPDATE_USER = gql`
    mutation UpdateUser($user: UserInput) {
        UpdateUser(user: $user){
            email
            favList {
                countryID
                countryName
            }
            visList {
                countryID
                countryName
            }
        }
    }
`;

export const ADD_FAVOURITE = gql`
mutation AddFavourite($email: String, $favourite: SavedCountryInput) {
    AddFavourite(email: $email, favourite: $favourite) {
      favList {
        countryID
        countryName
      }
    }
  }
`;


export const ADD_VISITED = gql`
mutation AddVisited($email: String, $visited: SavedCountryInput) {
    AddVisited(email: $email, visited: $visited) {
      email
      visList {
        countryID
        countryName
      }
    }
  }
`;
export const REMOVE_FAVOURITE = gql`
mutation RemoveFavourite($email: String, $favourite: SavedCountryInput) {
    RemoveFavourite(email: $email, favourite: $favourite) {
      favList {
        countryID
        countryName
      }
    }
  }
`;
export const REMOVE_VISITED = gql`
mutation RemoveVisited($email: String, $visited: SavedCountryInput) {
    RemoveVisited(email: $email, visited: $visited) {
      visList {
        countryID
        countryName
      }
    }
  }
`;