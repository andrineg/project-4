import { gql } from "@apollo/client";

//loads a single user
export const LOAD_USER = gql`
  query GetUser($email: String) {
    GetUser(email: $email) {
      email
      favList {
        countryID
        countryName
      }
      visList {
        countryID
        countryName
      }
    }
  }
`;

//loads all countries
export const LOAD_ALL_COUNTRIES = gql`
  query GetAllCountries($offset: Int, $limit: Int, $sort: SortBy, $search: String, $region: [String]) {
    GetAllCountries(offset: $offset, limit: $limit, sort: $sort, search: $search, region: $region) {
      countryID
      countryName
      region
      population
      area
      gdp
    }
  }
`;