/// <reference types="cypress" />

describe("Testing log out user", () => {
  beforeEach(() => {
    cy.visit("http://localhost:3000");
  });
  
    beforeEach(() => {
      cy.get("#inputEmail").type("test@example.com");
      cy.get("#submitEmail").click();
      cy.get("#HomeView").should("exist");
    });

    it("log out", () => {
      cy.wait(1000); // wait for 1 second
      cy.get("#logOutBtn").click();
      cy.wait(1000); // wait for 1 second
    });
  });