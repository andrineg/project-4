/// <reference types="cypress" />

describe("Testing log in user with both valied and un valied user", () => {
  beforeEach(() => {
    cy.visit("http://localhost:3000");
  });

  it("Check that app renders", () => {
    cy.get("#StartView").should("exist");
  });

  it("Inputs value to email- NOT valied email- field and click the button", () => {
    cy.get("#inputEmail").type("testtesttest");
    cy.get("#submitEmail").click();
  });

  it("Inputs value to email field and click the button", () => {
    cy.get("#inputEmail").type("test@example.com");
    cy.get("#submitEmail").click();
    cy.get("#HomeView").should("exist");
  });

});
