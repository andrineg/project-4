import { mergeTypeDefs, mergeResolvers } from '@graphql-tools/merge';
import { ApolloServer } from '@apollo/server';
import { startStandaloneServer } from '@apollo/server/standalone'
import { CountryResolver } from './resolvers/CountryResolver.js';
import { CountrySchema } from './schemas/CountrySchema.js';
import { UserSchema } from './schemas/UserSchema.js';
import { UserResolver } from './resolvers/UserResolver.js';
import { connect } from './database/Database.js';
import * as dotenv from "dotenv";
dotenv.config()

const typeDefs = mergeTypeDefs([CountrySchema, UserSchema]);
const resolvers = mergeResolvers([CountryResolver, UserResolver]);
const port = Number(process.env.PORT) || 8080;

connect();

const server = new ApolloServer({typeDefs, resolvers});

const { url } = await startStandaloneServer(server, {
    listen: {port: port, path: "/graphql"},
})

console.log(`Server Ready at: ${url}`);