import mongoose from "mongoose";
import UserModel, { IUser } from "../models/UserModel.js";

export const UserResolver = {
  Query:{
    GetUser: (parent, args, context, info)=>{
      const { email } = args;
      const query = {
        email: email
      }
      const update = {
      }
      const options = {
        new: true,
        upsert: true
      };
      return new Promise((resolve,reject) => {
        UserModel.findOneAndUpdate(query, update, options, (err: Error, user) => {
          if(err) reject(err);
          else resolve(user);
        })
      })
    }
  },
  Mutation:{
    UpdateUser:(parent, args, context, info) => {
      const  { user } = args;
      return new Promise((resolve,reject) =>{
        UserModel.findOneAndUpdate(user.email, user, {new: true, upsert: true}, (err:Error, user)=>{
        if(err) reject(err);
        else resolve(user)
        })
      })
    },

    AddFavourite: (parent, args, context, info) => {
      const { email, favourite } = args;

      return new Promise((resolve, reject) => {
        const query = {
          email: email,
          "favList.countryID": {
            $ne: favourite.countryID
          },
        };
        const update = {
          $push: {
             favList: favourite 
            } 
        };
        UserModel.findOneAndUpdate(query, update, { new: true }, (err: Error, user) => {
          if(err) {
            reject(err);
          }

          resolve(user);
        })
      })

    },
    AddVisited: (parent, args, context, info) => {
      const { email, visited } = args;

      return new Promise((resolve, reject) => {
        const query = {
          email: email,
          "visList.countryID": {
            $ne: visited.countryID
          }
        };
        const update = {
          $push: {
             visList: visited 
            } 
        };
        UserModel.findOneAndUpdate(query, update, { new: true }, (err: Error, user) => {
          if(err) {
            reject(err);
          }
          resolve(user);
        })
      })

    },
    RemoveFavourite: (parent, args, context, info) => {
      const { email, favourite } = args;

      return new Promise((resolve, reject) => {
        const query = {
          email: email
        };
        const remove = { 
          $pull: {
             favList: { countryID: favourite.countryID } 
            } 
        };
        UserModel.findOneAndUpdate(query, remove, { new: true }, (err: Error, user) => {
          if(err) {
            reject(err);
          }
          resolve(user);
        })
      })

    },
    RemoveVisited: (parent, args, context, info) => {
      const { email, visited } = args;

      return new Promise((resolve, reject) => {
        const query = {
          email: email
        };
        const remove = { 
          $pull: {
            visList: { countryID: visited.countryID } 
           } 
        };
        UserModel.findOneAndUpdate(query, remove, { new: true }, (err: Error, user) => {
          if(err) {
            reject(err);
          }

          resolve(user);
        })
      })

    }
  }
}