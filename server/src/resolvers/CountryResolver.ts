import { execFileSync } from "child_process";
import mongoose from "mongoose";
import CountryModel, { ICountry } from "../models/CountryModel.js";

export const CountryResolver = {
  Query:{
    GetAllCountries: (parent, args, context, info)=>{
        let options = {
          skip: parseInt(args.offset),
          limit: parseInt(args.limit)
        };
        let filter = {};
        if(args.sort && args.sort.order && args.sort.field) {
          options["sort"] =  (args.sort.order == "asc" ? "" : "-") + args.sort.field;
        }
        if(args.region && args.region.length != 0) {
          filter["region"] = {$in: args.region};
        }
        if(args.search) {
          if(!/^[a-zA-Z]+$/.test(args.search)) {
             return new Error("Invalid search string, can only be characters");
          }
          filter["countryName"] = {$regex: `.*${args.search}.*`}
        }
        try {
          return CountryModel.find(filter, null, options).exec();

        } catch (err) {
          return err
        }
    },
  } 
}