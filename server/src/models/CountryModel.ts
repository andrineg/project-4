import mongoose, { Schema, model } from "mongoose";

export interface ICountry extends mongoose.Document {
        countryID: Number,
        countryName: String,
        region: String,
        population: Number,
        area: Number,
        gdp: Number
        transform: () => ICountry;
}

const schema = new Schema<ICountry>({
        countryID: { type: Number, required: true, unique: true },
        countryName: { type: String, required: true },
        region: { type: String, required: true },
        population: { type: Number, required: true },
        area: { type: Number, required: true },
        gdp: { type: Number, required: true }
}, {collection: "countries"});

const CountryModel = model<ICountry>("Country", schema, "countries");

export default CountryModel;