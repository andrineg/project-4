import mongoose, { Schema, model } from "mongoose";

export interface IUser extends mongoose.Document {
  email: string,
  favList: [{
    countryID: Number,
    countryName: String
    }],
  visList: [{
    countryID: Number,
    countryName: String
    }]
  transform: () => IUser;
}

const schema = new Schema<IUser>({
  email: { type: String },
  favList: [{
    countryID: {type: Number, required: true},
    countryName: {type: String, required: true},
  }],
  visList: [{
    countryID: {type: Number, required: true},
    countryName: {type: String, required: true},
  }]
}, {collection: "savedUsers"});

const UserModel = model<IUser>("User", schema, "savedUsers");

export default UserModel