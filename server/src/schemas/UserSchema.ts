export const UserSchema = `#graphql  
type SavedCountry {
  countryID: Int
  countryName: String
}

type User {
  email: String
  favList: [SavedCountry]
  visList: [SavedCountry]
}

input SavedCountryInput {
  countryID: Int
  countryName: String
}

input UserInput {
  email: String
  favList: [SavedCountryInput]
  visList: [SavedCountryInput]
}

type Query {
  GetUser(email: String): User
}

type Mutation {
  UpdateUser(user: UserInput): User
  AddFavourite(email: String, favourite: SavedCountryInput): User
  AddVisited(email: String, visited: SavedCountryInput): User
  RemoveFavourite(email: String, favourite: SavedCountryInput): User
  RemoveVisited(email: String, visited: SavedCountryInput): User
}
`;