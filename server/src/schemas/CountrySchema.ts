export const CountrySchema = `#graphql 
  type Country {
    countryID: Int
    countryName: String
    region: String
    population: Int
    area: Int
    gdp: Int
  }

  enum Order {
    asc
    desc
  }

  input SortBy {
    field: String
    order: Order
  }

  type Query {
    GetAllCountries(offset: Int, limit: Int, sort: SortBy, search: String, region: [String]): [Country]
  }
`;